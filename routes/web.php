<?php

use App\Http\Controllers\GameController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/**
 * Game Routes
 */
Route::get('/games', [GameController::class, 'index']);

Route::get('/game/add', [GameController::class, 'create']);
Route::post('/game/add', [GameController::class, 'store']);

Route::get('game/{id}/details', [GameController::class, 'showGameDetail']);

Route::get('/app', function () {
    return view('app');
})->where('any', '.*');
