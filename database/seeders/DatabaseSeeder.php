<?php

namespace Database\Seeders;

use App\Models\Game;
use App\Models\Review;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Review::factory(3)->create();
        Game::factory(3)->create();
        User::factory(2)->create();

        User::factory()->create([
            'name' => 'Benno',
            'email' => 'b.eikelenboom@krm.nl',
        ]);
    }
}
