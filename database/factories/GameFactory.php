<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory
 */
class GameFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'name' => $this->faker->name(),
            'pegi' => $this->faker->numberBetween(3,18),
            'description' => $this->faker->text(100),
            'price' => $this->faker->randomNumber(2, false),
            'multiplayer' => $this->faker->boolean(),
        ];
    }
}
