<!Doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" value="{{ csrf_token() }}"/>

    <title>Game - detail</title>

    <link href="{{ mix('css/app.css') }}" type="text/css" rel="stylesheet"/>
</head>
<body>
<div id="app">
    <div>
        <div class="sm:w-1/2 lg:w-1/2 lg:p-8 m-auto">
            <div class="lg:w-2/4 sm:w-1/2 border float-left border-gray-300 border-2 leading-8">
                <p class="p-5">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                        Name
                    </label>
                    {{ $game->name }}
                </p>
                <p class="p-5">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                        PEGI age classification
                    </label>
                    {{ $game->pegi }}
                </p>
                <p class="p-5">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                        Price
                    </label>
                    €{{ $game->price }}
                </p>
                <p class="p-5">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                        Description
                    </label>
                    {{ $game->description }}
                </p>
            </div>
            <div class="lg:w-2/4 sm:w-1/2 float-right border border-gray-300 border-2 leading-8">
                <p class="p-3">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                        Reviews:
                    </label>
                    @foreach($game->reviews as $review)
                    <div class="m-3 border-b-2 pt-4">
                        <div>
                            <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                                @foreach($users as $user)
                                    @if($review->user_id === $user->id)
                                        Name: {{ $user->name }}
                                    @endif
                                @endforeach
                            </label>
                        </div>
                        <div>
                            <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                                Review:
                            </label>
                            {{ $review->comment }}
                        </div>
                    </div>
                    @endforeach
                </p>
            </div>
        </div>
        <div class="block m-auto">
            <a href="#">
                <button name="save" class="bg-white hover:bg-gray-100 text-gray-800 font-semibold py-2 px-4 border border-gray-400 rounded shadow">
                    Add Review
                </button>
            </a>
        </div>
    </div>
</div>
<script src="{{ mix('js/app.js') }}" type="text/javascript"></script>
</body>
</html>
