import { createApp } from 'vue'
import AddGame from './components/Forms/add-game-form'

const Vue = createApp({})

Vue.component('add-game-form', AddGame);

Vue.mount('#app')
