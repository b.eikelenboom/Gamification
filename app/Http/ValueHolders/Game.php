<?php

namespace App\Http\ValueHolders;

class Game
{
    public const NOT_FOUND = 'Game was not found';

    public function __construct(
        public int $id,
        public string $name,
        public int $pegi,
        public string $description,
        public string $price,
        public bool $multiplayer,
        public string $created_at,
        public string $updated_at
    )
    {
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getPegi(): int
    {
        return $this->pegi;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getPrice(): string
    {
        return $this->price;
    }

    /**
     * @return bool
     */
    public function isMultiplayer(): bool
    {
        return $this->multiplayer;
    }
}


