<?php

namespace App\Http\Repositories;

use App\Http\ValueHolders\Game;
use App\Models\Game as GameModel;
use Illuminate\Database\Eloquent\Collection;

class GameRepository
{
    public function getAll()
    {
        return new Collection(
            array_map(
                function ($array) {
                    return new Game(...$array);
                },
                json_decode(
                    GameModel::all(),
                    true
                )
            )
        );
    }
}
