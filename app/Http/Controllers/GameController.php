<?php

namespace App\Http\Controllers;

use App\Http\Repositories\GameRepository;
use App\Http\Resources\GameResource;
use App\Models\Game;
use App\Models\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View as ViewMake;

class GameController extends Controller
{
    public function __construct(
        private GameRepository $gameRepository
    ) {
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(): Response
    {
        $games = Game::all();
        return \response(
            [
                'games' => GameResource::collection($games),
                'message' => 'Retrieved a list of games'
            ], 200
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View
     */
    public function create(Request $request): View|Factory|Application
    {
        return view('game');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $token = $request->session()->token();

        //check incoming form data
        $validator = Validator::make($data,
            [
                'name' => 'required|min:5|max:50',
                'pegi' => 'required|numeric',
                'description' => 'required|max:255',
                'price' => 'required',
//                'multiplayer' => 'boolean'
            ]
        );

        if($validator->fails()) {
            return \response(
                [
                    'error' => $validator->errors(),
                    'message' => 'validation error'
                ]
            );
        }

        $multiplauyer = $request->input('multiplayer');
        if($multiplauyer) {
            $data['multiplayer'] = 1;
        } else {
            $data['multiplayer'] = 0;
        }

        $game = Game::create($data);

        return \response(
            [
                'game' => new GameResource($game),
                'message' => 'Successfully created a new game'
            ]
        );
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return Application|ResponseFactory|View|Response
     */
    public function showGameDetail($id): View|Response|Application|ResponseFactory
    {
        $game = Game::find($id);
        $user = User::all();

        if($game === null)
        {
            return \response(
                [
                    'error' => 'Game could not be found',
                    'message' => 'validation error'
                ]
            );
        }

        return ViewMake::make('detail-pages.gameDetail')->with('game', $game)->with('users', $user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Game $game
     * @return Response
     */
    public function edit(Game $game)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Game $game
     * @return Response
     */
    public function update(Request $request, Game $game)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Game $game
     * @return Response
     */
    public function destroy(Game $game)
    {
        //
    }

    public function getGames(): Collection
    {
        return $this->gameRepository->getAll();
    }
}
