<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Review extends Model
{
    use HasFactory;

    /**
     * Get the game that owns the review
     *
     * Syntax: return $this->belongsTo(Game::class, 'foreign_key', 'owner_key');
     *
     * Example: return $this->belongsTo(Game::class, 'game_id', íd');
     */
    public function game(): BelongsTo
    {
        return $this->belongsTo(Game::class);
    }
}
