<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Game extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'pegi', 'description', 'price', 'multiplayer'];

    /**
     * Get the reviews for the Game
     *
     * Syntax: return $this->hasMany(Review::class, 'foreign_key', 'local_key');
     *
     * Example: return $this->hasMany(Review::class, 'game_id', íd');
     */
    public function reviews(): HasMany
    {
        return $this->hasMany(Review::class);
    }
}
